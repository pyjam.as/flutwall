#include "FastLED.h"
#include <Arduino.h>
#include <WiFi.h>
#include <FreeRTOS.h>

#if FASTLED_VERSION < 3001000
#error "Requires FastLED 3.1 or later; check github for latest code."
#endif

#define DATA_PIN 4
#define LED_TYPE WS2812B
#define COLOR_ORDER GRB
#define NUM_LEDS 900
#define BRIGHTNESS 60
#define MAX_CLIENTS 10

#define FLASHING_MARIUS true // Flashing to marius speaker? (else flutwall)

#if FLASHING_MARIUS
//#define NUM_LEDS 510
#define COLOR_FACTOR 0.2
#define WALL_HEIGHT 16
#define WALL_WIDTH 32

int LED_MAP[16][32]{
    {15, 16, 47, 48, 79, 80, 111, 112, 143, 144, 175, 176, 207, 208, 239, 240, 271, 272, 303, 304, 335, 336, 367, 368, 398, 399, 430, 431, 461, 462, 493, 494},
    {14, 17, 46, 49, 78, 81, 110, 113, 142, 145, 174, 177, 206, 209, 238, 241, 270, 273, 302, 305, 334, 337, 366, 369, 397, 400, 429, 432, 460, 463, 492, 495},
    {13, 18, 45, 50, 77, 82, 109, 114, 141, 146, 173, 178, 205, 210, 237, 242, 269, 274, 301, 306, 333, 338, 365, 370, 396, 401, 428, 433, 459, 464, 491, 496},
    {12, 19, 44, 51, 76, 83, 108, 115, 140, 147, 172, 179, 204, 211, 236, 243, 268, 275, 300, 307, 332, 339, 364, 371, 395, 402, 427, 434, 458, 465, 490, 497},
    {11, 20, 43, 52, 75, 84, 107, 116, 139, 148, 171, 180, 203, 212, 235, 244, 267, 276, 299, 308, 331, 340, 363, 372, 394, 403, 426, 435, 457, 466, 489, 498},
    {10, 21, 42, 53, 74, 85, 106, 117, 138, 149, 170, 181, 202, 213, 234, 245, 266, 277, 298, 309, 330, 341, 362, 373, 393, 404, 425, 436, 456, 467, 488, 499},
    {9, 22, 41, 54, 73, 86, 105, 118, 137, 150, 169, 182, 201, 214, 233, 246, 265, 278, 297, 310, 329, 342, 361, 374, 392, 405, 424, 437, 455, 468, 487, 500},
    {8, 23, 40, 55, 72, 87, 104, 119, 136, 151, 168, 183, 200, 215, 232, 247, 264, 279, 296, 311, 328, 343, 360, 375, 391, 406, 423, 438, 454, 469, 486, 501},
    {7, 24, 39, 56, 71, 88, 103, 120, 135, 152, 167, 184, 199, 216, 231, 248, 263, 280, 295, 312, 327, 344, 359, 376, 390, 407, 422, 439, 453, 470, 485, 502},
    {6, 25, 38, 57, 70, 89, 102, 121, 134, 153, 166, 185, 198, 217, 230, 249, 262, 281, 294, 313, 326, 345, 358, 377, 389, 408, 421, 440, 452, 471, 484, 503},
    {5, 26, 37, 58, 69, 90, 101, 122, 133, 154, 165, 186, 197, 218, 229, 250, 261, 282, 293, 314, 325, 346, 357, 378, 388, 409, 420, 441, 451, 472, 483, 504},
    {4, 27, 36, 59, 68, 91, 100, 123, 132, 155, 164, 187, 196, 219, 228, 251, 260, 283, 292, 315, 324, 347, 356, 379, 387, 410, 419, 442, 450, 473, 482, 505},
    {3, 28, 35, 60, 67, 92, 99, 124, 131, 156, 163, 188, 195, 220, 227, 252, 259, 284, 291, 316, 323, 348, 355, 380, 386, 411, 418, 443, 449, 474, 481, 506},
    {2, 29, 34, 61, 66, 93, 98, 125, 130, 157, 162, 189, 194, 221, 226, 253, 258, 285, 290, 317, 322, 349, 354, 381, 385, 412, 417, 444, 448, 475, 480, 507},
    {1, 30, 33, 62, 65, 94, 97, 126, 129, 158, 161, 190, 193, 222, 225, 254, 257, 286, 289, 318, 321, 350, 353, 382, 384, 413, 416, 445, 447, 476, 479, 508},
    {0, 31, 32, 63, 64, 95, 96, 127, 128, 159, 160, 191, 192, 223, 224, 255, 256, 287, 288, 319, 320, 351, 352, 383, 383, 414, 415, 446, 446, 477, 478, 509},
};

IPAddress local_IP(100, 68, 0, 3);
#else
//#define NUM_LEDS 500
#define COLOR_FACTOR 1
#define WALL_WIDTH 20
#define WALL_HEIGHT 20

int LED_MAP[20][20] = {
    {419, 416, 375, 372, 331, 328, 286, 283, 242, 239, 198, 195, 154, 151, 110, 107, 66, 63, 22, 19},
    {420, 415, 376, 371, 332, 327, 287, 282, 243, 238, 199, 194, 155, 150, 111, 106, 67, 62, 23, 18},
    {421, 414, 377, 370, 333, 326, 288, 281, 244, 237, 200, 193, 156, 149, 112, 105, 68, 61, 24, 17},
    {422, 413, 378, 369, 334, 325, 289, 280, 245, 236, 201, 192, 157, 148, 113, 104, 69, 60, 25, 16},
    {423, 412, 379, 368, 335, 324, 290, 279, 246, 235, 202, 191, 158, 147, 114, 103, 70, 59, 26, 15},
    {424, 411, 380, 367, 336, 323, 291, 278, 247, 234, 203, 190, 159, 146, 115, 102, 71, 58, 27, 14},
    {425, 410, 381, 366, 337, 322, 292, 277, 248, 233, 204, 189, 160, 145, 116, 101, 72, 57, 28, 13},
    {426, 409, 382, 365, 338, 321, 293, 276, 249, 232, 205, 188, 161, 144, 117, 100, 73, 56, 29, 12},
    {427, 408, 383, 364, 339, 320, 294, 275, 250, 231, 206, 187, 162, 143, 118, 99, 74, 55, 30, 11},
    {428, 407, 384, 363, 340, 319, 295, 274, 251, 230, 207, 186, 163, 142, 119, 98, 75, 54, 31, 10},
    {429, 406, 385, 362, 341, 318, 296, 273, 252, 229, 208, 185, 164, 141, 120, 97, 76, 53, 32, 9},
    {430, 405, 386, 361, 342, 317, 297, 272, 253, 228, 209, 184, 165, 140, 121, 96, 77, 52, 33, 8},
    {431, 404, 387, 360, 343, 316, 298, 271, 254, 227, 210, 183, 166, 139, 122, 95, 78, 51, 34, 7},
    {432, 403, 388, 359, 344, 315, 299, 270, 255, 226, 211, 182, 167, 138, 123, 94, 79, 50, 35, 6},
    {433, 402, 389, 358, 345, 314, 300, 269, 256, 225, 212, 181, 168, 137, 124, 93, 80, 49, 36, 5},
    {434, 401, 390, 357, 346, 313, 301, 268, 257, 224, 213, 180, 169, 136, 125, 92, 81, 48, 37, 4},
    {435, 400, 391, 356, 347, 312, 302, 267, 258, 223, 214, 179, 170, 135, 126, 91, 82, 47, 38, 3},
    {436, 399, 392, 355, 348, 311, 303, 266, 259, 222, 215, 178, 171, 134, 127, 90, 83, 46, 39, 2},
    {437, 398, 393, 354, 349, 310, 304, 265, 260, 221, 216, 177, 172, 133, 128, 89, 84, 45, 40, 1},
    {438, 397, 394, 353, 350, 309, 305, 264, 261, 220, 217, 176, 173, 132, 129, 88, 85, 44, 41, 0}};

IPAddress local_IP(100, 68, 0, 2);
#endif

CRGB leds[NUM_LEDS];

TaskHandle_t BUTTON_LISTENER; // Define RTOS Task Handles
TaskHandle_t RECONNECT;
TaskHandle_t MQTT_TASK;
TaskHandle_t PACIFICA_LOOP_TASK;
TaskHandle_t PRIDE_LOOP_TASK;
TaskHandle_t FLUT_LOOP;

WiFiServer Server(1337);

// Set your Gateway IP address
IPAddress gateway(100, 68, 0, 1);

IPAddress subnet(255, 255, 255, 0);
IPAddress primaryDNS(1, 1, 1, 1);   //optional
IPAddress secondaryDNS(8, 8, 8, 8); //optional

const char *ssid = "bornhack-pyjam";
const char *password = "";

String payload;
void callback(char *byteArraytopic, byte *byteArrayPayload, unsigned int length);
WiFiClient espClient;

void setup_wifi()
{
  // Configures static IP address
  if (!WiFi.config(local_IP, gateway, subnet, primaryDNS, secondaryDNS))
  {
    Serial.println("STA Failed to configure");
  }

  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void setup()
{
  Serial.begin(9600);

  Serial.println("Hello, World!");

  // Init LEDS:
  FastLED.addLeds<LED_TYPE, DATA_PIN, COLOR_ORDER>(leds, NUM_LEDS)
      .setCorrection(TypicalLEDStrip)
      .setDither(BRIGHTNESS < 255);
  FastLED.setBrightness(BRIGHTNESS);

  setup_wifi();
  Server.begin();

  xTaskCreatePinnedToCore(flut_loop, "Flut_loop", 4 * 1024, NULL, 1, &FLUT_LOOP, 1);
}

char mygetchar(WiFiClient *remoteClient)
{
  uint8_t c;
  remoteClient->read(&c, 1);
  return c;
}

int read_number(int max_number_len, int base, char stop_char, WiFiClient *remoteClient)
{
  int number = 0;
  for (int i = 0; i < max_number_len + 1; i++)
  {
    int mychar = (int)mygetchar(remoteClient);
    if (mychar >= 0x30 && mychar <= 0x39)
    {
      number = number * base + (mychar - 0x30);
    }
    else if (base == 16 && mychar >= 0x41 && mychar <= 0x46)
    {
      number = number * base + (mychar - 0x37);
    }
    else if (base == 16 && mychar >= 0x61 && mychar <= 0x66)
    {
      number = number * base + (mychar - 0x57);
    }
    else if (mychar == stop_char)
    {
      return number;
    }
    else
    {
      return -1;
    }
  }
  return -1;
}

void set_pixel(int x, int y, int color)
{
  int red = ((color >> 16) & 0xff) * COLOR_FACTOR;
  int green = ((color >> 8) & 0xff) * COLOR_FACTOR;
  int blue = (color & 0xff) * COLOR_FACTOR;

  //printf("%d %d -> %d%d%d\n", x, y, red, green, blue);

  CRGB newcolor = CRGB(red, green, blue);

  leds[LED_MAP[y][x]] = newcolor;
}

WiFiClient *clients[MAX_CLIENTS] = {NULL};

void client_loop(void *parameter)
{
  int client_index = *(int *)parameter;
  WiFiClient *remoteClient = clients[client_index];
  while (remoteClient->connected())
  {
    while (remoteClient->available())
    {
      char c = mygetchar(remoteClient);
      if (c == 'P')
      {
        if (mygetchar(remoteClient) != 'X')
          continue;
        if (mygetchar(remoteClient) != ' ')
          continue;
        int x = read_number(2, 10, ' ', remoteClient);
        if (x == -1 || x >= WALL_WIDTH)
          continue;
        int y = read_number(2, 10, ' ', remoteClient);
        if (y == -1 || y >= WALL_HEIGHT)
          continue;
        int color = read_number(6, 16, '\n', remoteClient);
        if (color == -1)
          continue;
        set_pixel(x, y, color);
      }
    }
  }
  printf("Killing thread properly\n");
  remoteClient->flush();
  remoteClient->stop();
  clients[client_index] = NULL;
  vTaskDelete(NULL);
}

int client_count = 0;
void flut_loop(void *pvParameters)
{
  int last_time = millis();
  while (1)
  {
    WiFiClient newClient = Server.available();
    if (newClient)
    {
      for (int i = 0; i < MAX_CLIENTS; ++i)
      {
        if (NULL == clients[i])
        {
          printf("Got new client\n");
          int client_name_char_count = 14 + int(log(i + 1)) / log(10);
          char *client_name = (char *)malloc(client_name_char_count * sizeof(char));
          sprintf(client_name, "client_loop_%d", i + 1);
          clients[i] = new WiFiClient(newClient);
          printf("Spawning thread '%s'\n", client_name);
          xTaskCreate(client_loop, client_name, 4*1024, (void *)&i, 2, NULL);
          break;
        } else if (i-1 == MAX_CLIENTS){
          printf("No more client slots left");
        }
      }
    }

    if ((millis() - last_time) > 33)
    {
      //printf("Setting pixels\n");
      last_time = millis();
      FastLED.show();
    }
  }
}

void loop()
{
}
